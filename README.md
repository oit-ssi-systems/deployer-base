# deployer-base

This project creates a base image that will be stored in the projects container
registry. This base image will be used by the PCS project's CI/CD process.  By
installing software in the base image the CI pipeline to deploy new code will
be quicker as there will be less to do there.

## Tagging

Using specific version tags will keep the correct version in the gitlab runner
cache, so it doesn't need to be re-downloaded every run.  To tag a new version,
use something like:

```
git tag 1.0.0
git push --tags
```

`master` will always map to `latest`
