FROM ubuntu:20.04
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      git python3 python3-yaml python3-requests gpg-agent\
      python3-hvac ansible puppet puppet-lint \
      openssh-client software-properties-common curl && \
    curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
    apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
    apt-get update && \
    apt-get -y install vault && \
    rm -rf /var/lib/apt/lists/*

COPY vault-approle-get-token.py /usr/local/bin/

COPY generate_ssh_key.py /usr/local/bin/
