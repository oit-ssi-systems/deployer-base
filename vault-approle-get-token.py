#!/usr/bin/env python3
import sys
import argparse
import os
import yaml
import requests
import json


def parse_args():
    parser = argparse.ArgumentParser(
        description='Get a Vault Token using AppRole info')

    identity_group = parser.add_argument_group('Identity related arguments')
    identity_group.add_argument(
        '-i', '--identity-file',
        help="path to yaml file with your role-id, secret-id and vault-addr",
        type=argparse.FileType('r'),
        required=False
    )
    identity_group.add_argument(
        '-r', '--role-id', type=str,
        default=os.environ.get('VAULT_ROLE_ID'),
        help="role-id or use VAULT_ROLE_ID environment variable")
    identity_group.add_argument(
        '-s', '--secret-id', type=str,
        default=os.environ.get('VAULT_SECRET_ID'),
        help="secret-id or use VAULT_SECRET_ID environment variable")
    identity_group.add_argument(
        '-a', '--vault-addr', type=str,
        default=os.environ.get('VAULT_ADDR'),
        help="vault-addr or use VAULT_ADDR environment variable")

    return parser.parse_args()


def assemble_identity(args):

    # Determine if we are using an identity file
    if 'VAULT_IDENTITY_FILE' in os.environ:
        identity_file = os.environ['VAULT_IDENTITY_FILE']
    elif args.identity_file:
        identity_file = args.identity_file.name
    elif os.path.exists(os.path.expanduser("~/.vault_role.yaml")):
        identity_file = os.path.expanduser("~/.vault_role.yaml")
    else:
        identity_file = None

    if (args.vault_addr and args.role_id and args.secret_id):

        identity_info = {
                'vault_addr': args.vault_addr,
                'role_id': args.role_id,
                'secret_id': args.secret_id
                }
        return identity_info

    elif identity_file:

        if os.path.exists(identity_file):
            identity_info = yaml.load(open(identity_file, 'r'))
            return identity_info
        else:
            sys.stderr.write("Identity file not found")
            sys.exit(2)

    else:
        sys.stderr.write(
                "Not able to find all credentials in environment or file\n")
        sys.exit(2)


def main():
    args = parse_args()
    identity_info = assemble_identity(args)

    # Log in to the approle
    login = requests.post(
        "%s/v1/auth/approle/login" % identity_info['vault_addr'],
        json={
            'role_id': identity_info['role_id'],
            'secret_id': identity_info['secret_id']
        }
    )

    try:
        token = login.json()['auth']['client_token']
    except Exception:
        sys.stderr.write(
            "Could not get client token, please check your credentials\n")
        sys.exit(2)

    sys.stdout.write(token)
    return 0


if __name__ == "__main__":
    sys.exit(main())
