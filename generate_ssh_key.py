#!/usr/bin/env python3
import sys
import hvac
import os
import hvac
import subprocess


def main():

    # Vault connection with local token
    vault = hvac.Client(url=os.environ.get('VAULT_ADDR'))
    if 'VAULT_TOKEN' in os.environ:
        sys.stderr.write("Using VAULT_TOKEN auth\n")
        vault.token = os.environ['VAULT_TOKEN']

    if not os.path.exists("/root/.ssh/id_ed25519"):
        print("Creating ed25519 key")
        subprocess.check_call(
            ["ssh-keygen", "-t", "ed25519", "-N", "", "-f", "/root/.ssh/id_ed25519"])

    with open('/root/.ssh/id_ed25519.pub', 'r') as ed25519_pub:
        pub_key = ed25519_pub.read()
        res = vault.write('ssh-client-signer/sign/deployer',
                          public_key=pub_key)['data']['signed_key']
        with open('/root/.ssh/id_ed25519.pub.signed', 'w') as ed25519_pub_signed:
            ed25519_pub_signed.write(res)

    return 0


if __name__ == "__main__":
    sys.exit(main())
